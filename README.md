# Utiles

## Archivos
- bootloader.bin - bootloader del proyecto stm32duino
- obc-sch.pdf - esquemático placa CSB01
- eps-sch.pdf - esquemático placa CSB02
- comm-sch.pdf - esquemático placa CSB03

## Links

### Arduino
https://www.arduino.cc/

https://github.com/rogerclarkmelbourne/Arduino_STM32/wiki (compatibilidad arduino | proyecto alternativo | recomendado!!)

https://github.com/stm32duino/ (compatibilidad arduino | proyecto oficial)

### ARM Cortex
https://www.st.com/en/development-tools/stm32cubemx.html (configuración ARM Cortex-M)

### RTL-SDR
https://www.rtl-sdr.com/

https://www.sdr-radio.com/ (analizador de espectro)

https://www.mathworks.com/hardware-support/rtl-sdr.html (extensión SDR para Matlab)

https://www.youtube.com/playlist?list=PL8bSwVy8_IcPCsBE71CYBLbQSS8ckWm6x (curso receptor SDR)


### Herramientas

https://www.st.com/en/development-tools/stsw-link004.html (ST-Link V2)

https://www.silabs.com/products/development-tools/software/wireless-development-suite (configuración radio)

http://ioninja.com/ (terminal de proposito general)

https://github.com/jopohl/urh/ (analizador de protocolos)